#!/bin/bash
#
# Copyright (C) 2023, Renesas Electronics Europe GmbH, Chris Paterson
# <chris.paterson2@renesas.com>
#
# This script gets fetches the current kernel version name
#
################################################################################

MAJOR_KERNEL_VERSION=$(make kernelversion | cut -d '.' -f -2)
KERNEL_NAME="linux-${MAJOR_KERNEL_VERSION}.y"

# Check for CIP kernel
if [ -f localversion-cip ]; then
	KERNEL_NAME="${KERNEL_NAME}-cip"
fi

# Check for CIP RT kernel
if [ -f localversion-cip-rt ]; then
	KERNEL_NAME="${KERNEL_NAME}-rt"
fi

echo "${KERNEL_NAME}"
