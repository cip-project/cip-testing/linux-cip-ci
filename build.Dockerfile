FROM ubuntu:18.04
MAINTAINER Chris Paterson <chris.paterson2@renesas.com>

ENV DEBIAN_FRONTEND noninteractive

# Install dependencies
RUN apt-get update \
&& apt-get install -y --no-install-recommends apt-utils git build-essential \
libncurses-dev bison flex libssl-dev libelf-dev bc u-boot-tools wget kmod \
ca-certificates libssl-dev liblz4-tool lzop curl python3 \
&& rm -rf /var/lib/apt/lists/*

# Clone cip-kernel-config repository
RUN git clone https://gitlab.com/cip-project/cip-kernel/cip-kernel-config.git \
/opt/cip-kernel-config

# Copy build script
COPY build_kernel.sh /opt/
RUN chmod +x /opt/build_kernel.sh
RUN sed -i -e "s/__USE_DEBIAN_CROSS_COMPILER__//g" /opt/build_kernel.sh

# Copy build report script
COPY build_report.sh /opt/
RUN chmod +x /opt/build_report.sh

# Copy stable commit script
COPY create_stable_commit.sh /opt/
RUN chmod +x /opt/create_stable_commit.sh

# Copy kernel name script
COPY get_kernel_version_name.sh /opt/
RUN chmod +x /opt/get_kernel_version_name.sh
