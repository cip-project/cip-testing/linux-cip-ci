#!/bin/bash
#
# Copyright (C) 2020, Renesas Electronics Europe GmbH, Chris Paterson
# <chris.paterson2@renesas.com>
#
# This script takes a given architecture and configuration and installs the
# required compiler and builds the Kernel with it, ready for testing.
#
# Compiler installation influenced from the work done by Hiramatsu-san at:
# https://github.com/mhiramat/linux-cross
#
# Script specific dependencies:
# wget uname nproc make tar pwd sed
#
# Global environment variables used by this script:
# BUILD_ARCH: The architecture to build for. Architectures currently supported
#             are: arm, arm64, powerpc, riscv, x86.
# CONFIG:     The name of the configuration file to be used. Can be in either
#             .config or defconfig format.
# CONFIG_LOC: Must be one of the following options:
#             * intree: Configuration is present in the linux-cip Kernel.
#             * cip-kernel-configs: Configuration is present in the
#               https://gitlab.com/cip-project/cip-kernel/cip-kernel-config
#               repository. The master branch will be used unless
#               CIP_KERNEL_CONFIGS_BRANCH is set to something else.
#             * url: Download configuration from public URL set by an additional
#               variable called CONFIG_URL. The link should be to the directory
#               where the config is stored, not the actual file.
# GCC_VER:    Optionally set to overwrite the GCC version used.
#             Default is v8.1.0.
#
################################################################################

set -ex

################################################################################
WORK_DIR=$(pwd)
GCC_VER=${GCC_VER:="8.1.0"}
COMPILER_BASE_URL="https://cdn.kernel.org/pub/tools/crosstool/files/bin"
COMPILER_INSTALL_DIR="$WORK_DIR/gcc"
MODULE_INSTALL_DIR="modules"	# To be appended to $TMP_DIR
OUTPUT_DIR="$WORK_DIR/output"
USE_DEBIAN_CROSS_COMPILER="__USE_DEBIAN_CROSS_COMPILER__"
################################################################################
CPUS=$(nproc)
HOST_ARCH=$(uname -m)
################################################################################

set_up () {
	TMP_DIR="$(mktemp -d)"
	MODULE_INSTALL_DIR=$TMP_DIR/$MODULE_INSTALL_DIR
	mkdir -p "$COMPILER_INSTALL_DIR"
	mkdir -p "$MODULE_INSTALL_DIR"
	mkdir -p "$OUTPUT_DIR"

	# Fixup lzma issue
	if [ ! -f /usr/bin/lzma ] ; then
		update-alternatives --auto lzma
	fi
}

clean_up () {
	rm -rf "$TMP_DIR"
}

clean_build () {
	make mrproper
}

configure_special_cases () {
	case "$CONFIG" in
		"shmobile_defconfig")
			# This config prefers uImage
			BUILD_FLAGS="$BUILD_FLAGS LOADADDR=0x40008000"
			IMAGE_TYPE="${IMAGE_TYPE} uImage"
			;;
		"renesas_shmobile_defconfig")
			# This config prefers uImage
			BUILD_FLAGS="$BUILD_FLAGS LOADADDR=0x40008000"
			IMAGE_TYPE="${IMAGE_TYPE} uImage"
			;;
		"renesas_shmobile-rt_defconfig")
			# This config prefers uImage
			BUILD_FLAGS="$BUILD_FLAGS LOADADDR=0x40008000"
			IMAGE_TYPE="${IMAGE_TYPE} uImage"
			;;
		"multi_v7_defconfig")
			# This config prefers uImage
			BUILD_FLAGS="$BUILD_FLAGS LOADADDR=0x40008000"
			IMAGE_TYPE="${IMAGE_TYPE} uImage"
			echo "CONFIG_RAVB=y" >> arch/"$BUILD_ARCH"/configs/"$CONFIG"
			;;
		"siemens_am335x-dxr2_defconfig")
			# Remove power management firmware
			# (we don't want to re-distribute)
			sed -i '/CONFIG_EXTRA_FIRMWARE="am335x-pm-firmware.bin"/d' \
				arch/"$BUILD_ARCH"/configs/"$CONFIG"
			;;
		"siemens_am335x-draco_defconfig")
			# Remove power management firmware
			# (we don't want to re-distribute)
			sed -i '/CONFIG_EXTRA_FIRMWARE="am335x-pm-firmware.bin"/d' \
				arch/"$BUILD_ARCH"/configs/"$CONFIG"
			;;
		"siemens_am335x-etamin_defconfig")
			# Remove power management firmware
			# (we don't want to re-distribute)
			sed -i '/CONFIG_EXTRA_FIRMWARE="am335x-pm-firmware.bin"/d' \
				arch/"$BUILD_ARCH"/configs/"$CONFIG"
			;;
		"toshiba_defconfig")
			if [ $GCC_VER == "8.1.0" ] && [ "$BUILD_ARCH" == "powerpc" ]; then
				echo "CONFIG_PPC_DISABLE_WERROR=y" >> arch/"$BUILD_ARCH"/configs/"$CONFIG"
			fi
	esac
}

get_kernel_name () {
	# Work out Kernel version
	local sha=$(git log --pretty=format:"%h" -1)
	local version=$(make kernelversion)

	# Check for local version
	for localversionfile in localversion*; do
		if [ -f "$localversionfile" ]; then
			local localversion=$(cat "$localversionfile")
			version=${version}${localversion}
		fi
	done
	version=${version}_${sha}

	# Define Kernel image name
	KERNEL_NAME=${CONFIG}_${version}
}

save_kernel_config () {
	# Copy Kernel config now so that we have it even if the build fails
	local bin_dir=$KERNEL_NAME/$BUILD_ARCH/$CONFIG
	mkdir -p "$OUTPUT_DIR"/"$bin_dir"/config
	cp .config "$OUTPUT_DIR"/"$bin_dir"/config
}

configure_kernel () {
	if [ -z "$CONFIG" ]; then
		echo "No config provided. Using \"defconfig\"."
		CONFIG="defconfig"
	fi

	case "$CONFIG_LOC" in
		"")
			echo "No config location provided. Assuming \"intree\"..."
			# Fall-through to "intree"
			;&

		"intree")
			configure_special_cases
			;;

		"cip-kernel-config")
			# Update repo
			cd /opt/cip-kernel-config
			git fetch origin
			git reset --hard origin/${CIP_KERNEL_CONFIGS_BRANCH} || \
				git reset --hard origin/master
			cd -

			# Check provided config is there
			local ver=$(/opt/get_kernel_version_name.sh | cut -f2- -d -)
			if [ ! -f /opt/cip-kernel-config/"${ver}"/"$BUILD_ARCH"/$CONFIG ]; then
				# If RT version, fallback to non-RT and search again
				if [[ "${ver}" == *"-rt" ]]; then
					ver="${ver%-rt}"

					if [ ! -f /opt/cip-kernel-config/"${ver}"/"$BUILD_ARCH"/$CONFIG ]; then
						echo "Error: Provided configuration not present in cip-kernel-configs (RT or non-RT)"
						clean_up
						exit 1
					else
						echo "Config not found in ${ver}-rt, using config found in ${ver} instead"
					fi
				else
					echo "Error: Provided configuration not present in cip-kernel-configs"
					clean_up
					exit 1
				fi
			fi

			# Copy config
			# Note that if there are multiple configurations with
			# the same name in *.y-cip and *.y-cip-rt this will fail
			if [[ $CONFIG == *.config ]]; then
				cp /opt/cip-kernel-config/"${ver}"/"$BUILD_ARCH"/"${CONFIG}" .config
			else
				# Assume defconfig
				cp /opt/cip-kernel-config/"${ver}"/"$BUILD_ARCH"/"${CONFIG}" arch/"$BUILD_ARCH"/configs/
			fi

			configure_special_cases
			;;

		"url")
			# Download config
			if [ -z "$CONFIG_URL" ]; then
				echo "No config URL provided"
				clean_up
				exit 1
			fi

			if [[ $CONFIG == *.config ]]; then
				wget -q "$CONFIG_URL"/$CONFIG -O .config
				if [ $? -ne 0 ]; then
					echo "Error: Config file download failure"
					clean_up
					exit 1
				fi
			else
				# Assume defconfig
				wget -q -P arch/"$BUILD_ARCH"/configs/ "$CONFIG_URL"/$CONFIG
				if [ $? -ne 0 ]; then
					echo "Error: Config file download failure"
					clean_up
					exit 1
				fi
			fi

			# Configure special cases. Obviously this is on the luck
			# that the config names match up.
			# TODO: Add another way to configure BUILD_FLAGS etc.,
			# probably by board rather than config.
			configure_special_cases
			;;
	esac

	# Temporary fix library search problem,
	# make -j16 ARCH=arm64 'CROSS_COMPILE=/builds/hVatwYBy/2/cip-project/cip-testing/linux-cip-ci/gcc/gcc-*/aarch64-linux/bin/aarch64-linux-' renesas_defconfig
	# HOSTCC  scripts/basic/fixdep
	# /usr/lib/gcc/x86_64-linux-gnu/8/cc1: error while loading shared libraries: libmpc.so.3: cannot open shared object file: No such file or directory
	ldconfig

	if [[ $CONFIG == *.config ]]; then
		yes '' | make $BUILD_FLAGS oldconfig
	else
		# Assume defconfig
		make $BUILD_FLAGS $CONFIG
	fi

	get_kernel_name
	save_kernel_config
}

build_modules () {
	# Make sure install environment is clean
	rm -rf "$TMP_DIR"/modules.tar.gz
	rm -rf "$MODULE_INSTALL_DIR"

	make $BUILD_FLAGS modules
	make $BUILD_FLAGS modules_install INSTALL_MOD_PATH="$MODULE_INSTALL_DIR"

	# Package up for distribution
	tar -C "${MODULE_INSTALL_DIR}" -czf "$TMP_DIR"/modules.tar.gz lib
}

build_dtbs () {
	make $BUILD_FLAGS dtbs
}

build_kernel () {
	BUILD_MODULES=false

	make $BUILD_FLAGS $IMAGE_TYPE

	if grep -qc "CONFIG_MODULES=y" .config; then
		BUILD_MODULES=true
		build_modules
	fi

	if $BUILD_DTBS; then
		build_dtbs
	fi
}

# TODO: Make sure docker image installs the compilers as well
install_compiler () {
	local ext=".tar.gz"
	local gcc_file="$HOST_ARCH-gcc-$GCC_VER-nolibc-$GCC_NAME$ext"

	wget -q -P "$TMP_DIR"/ $COMPILER_BASE_URL/"$HOST_ARCH"/$GCC_VER/"$gcc_file"
	if [ $? -ne 0 ]; then
		echo "Error: Compiler download failure"
		clean_up
		exit 1
	fi

	tar xf "$TMP_DIR"/"$gcc_file" -C "$COMPILER_INSTALL_DIR"
}

configure_compiler () {
	local compiler_exec="${GCC_NAME}"-gcc
	if [ -z "${USE_DEBIAN_CROSS_COMPILER}" ] ; then
		compiler_exec=("${COMPILER_INSTALL_DIR}"/gcc-*/"${GCC_NAME}"/bin/"${GCC_NAME}"-gcc)
		[[ -x $compiler_exec ]] || install_compiler
	fi

	BUILD_FLAGS="-j$CPUS ARCH=$BUILD_ARCH CROSS_COMPILE=${compiler_exec%gcc}"
}

configure_arch () {
	case "$BUILD_ARCH" in
		"arm")
			if [ -z "${USE_DEBIAN_CROSS_COMPILER}" ] ; then
				GCC_NAME="arm-linux-gnueabi"
			else
				GCC_NAME="arm-linux-gnueabihf"
			fi
			IMAGE_TYPE="zImage"
			BUILD_DTBS=true
			;;
		"arm64")
			if [ -z "${USE_DEBIAN_CROSS_COMPILER}" ] ; then
				GCC_NAME="aarch64-linux"
			else
				GCC_NAME="aarch64-linux-gnu"
			fi
			IMAGE_TYPE="Image"
			BUILD_DTBS=true
			;;
		"powerpc")
			if [ -z "${USE_DEBIAN_CROSS_COMPILER}" ] ; then
				GCC_NAME="powerpc-linux"
			else
				GCC_NAME="powerpc-linux-gnu"
			fi
			IMAGE_TYPE="zImage"
			BUILD_DTBS=false
			;;
		"riscv")
			if [ -z "${USE_DEBIAN_CROSS_COMPILER}" ] ; then
				GCC_NAME="riscv64-linux"
			else
				GCC_NAME="riscv64-linux-gnu"
			fi
			IMAGE_TYPE="Image"
			BUILD_DTBS=true
			;;
		"x86")
			if [ -z "${USE_DEBIAN_CROSS_COMPILER}" ] ; then
				GCC_NAME="i386-linux"
			else
				GCC_NAME="x86_64-linux-gnu"
			fi
			IMAGE_TYPE="bzImage"
			BUILD_DTBS=false
			;;
		"")
			echo "Error: No target architecture provided"
			clean_up
			exit 1
			;;
		*)
			echo "Error: Target architecture not supported"
			clean_up
			exit 1
			;;
	esac
}

configure_build () {
	configure_arch
	configure_compiler
	configure_kernel
}

copy_output () {
	local bin_dir=$KERNEL_NAME/$BUILD_ARCH/$CONFIG

	# Kernel
	mkdir -p "$OUTPUT_DIR"/"$bin_dir"/kernel
	for image in ${IMAGE_TYPE}; do
		cp arch/"$BUILD_ARCH"/boot/$image "$OUTPUT_DIR"/"$bin_dir"/kernel
	done

	# Modules
	if $BUILD_MODULES; then
		mkdir -p "$OUTPUT_DIR"/"$bin_dir"/modules
		cp "$TMP_DIR"/modules.tar.gz "$OUTPUT_DIR"/"$bin_dir"/modules
	fi

	# Copy all device trees created by build config
	mkdir -p "$OUTPUT_DIR"/"$bin_dir"/dtb
	find ./arch -name \*.dtb -exec cp {} "$OUTPUT_DIR"/"$bin_dir"/dtb \;
}


trap clean_up SIGHUP SIGINT SIGTERM
set_up

configure_build
build_kernel
copy_output

clean_up
