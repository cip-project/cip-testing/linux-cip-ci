FROM ubuntu:18.04
MAINTAINER Chris Paterson <chris.paterson2@renesas.com>

ENV DEBIAN_FRONTEND noninteractive

# Install dependencies
RUN apt-get update \
&& apt-get install -y --no-install-recommends apt-utils lavacli curl unzip \
&& rm -rf /var/lib/apt/lists/*

# Install AWS CLI
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
&& unzip -q awscliv2.zip \
&& ./aws/install

# Copy lava templates
COPY lava_templates /opt/lava_templates

# Copy test script
COPY submit_tests.sh /opt/
RUN chmod +x /opt/submit_tests.sh
