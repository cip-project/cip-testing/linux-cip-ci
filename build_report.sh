#!/bin/bash
# Copyright (C) 2023, Robert Bosch GmbH, Sietze van Buuren
# <sietze.vanbuuren@de.bosch.com>
#
# This script reports to SQUAD whether or not the a kernel build was successful
#
#
# The following must be set in GitLab CI variables for the SQUAD curl command to work:
# $CIP_SQUAD_LAB_TOKEN
# $CIP_SQUAD_URL (default: https://squad.ciplatform.org)
# $CIP_SQUAD_GROUP (default: cip-kernel)
#
# Global environment variables used by this script:
# CONFIG:       The name of the configuration file used in the build job.
#               Required to help with directory navigation.

################################################################################

set -e

# Return if the SQUAD TOKEN hasn't been set
if [ -z ${CIP_SQUAD_LAB_TOKEN+x} ]; then
	echo "CIP_SQUAD_LAB_TOKEN not found, omitting SQUAD results reporting!"
	exit 0
fi

# Return if we're on a rebase branch/commit
if [[ $CI_COMMIT_REF_NAME == *"-rebase" ]]; then
	echo "This is a rebase branch/commit, omitting SQUAD results reporting!"
	exit 0
fi

################################################################################
WORK_DIR=$(pwd)
OUTPUT_DIR="$WORK_DIR/output"
VERSION=$(ls "${OUTPUT_DIR}")
################################################################################
SQUAD_URL="${CIP_SQUAD_URL:-https://squad.ciplatform.org}"
SQUAD_SUBMIT_URL="${SQUAD_URL}/api/submit"
################################################################################
SQUAD_GROUP="${CIP_SQUAD_GROUP:-cip-kernel}"
SQUAD_PROJECT=$(/opt/get_kernel_version_name.sh | tee ${OUTPUT_DIR}/${VERSION}/kernel_name)
################################################################################
BUILD=$(echo $VERSION | sed "s/${CONFIG}_//")
ENV=${BUILD_ARCH}_${CONFIG}

squad_url="${SQUAD_SUBMIT_URL}/${SQUAD_GROUP}/${SQUAD_PROJECT}/${BUILD}/${ENV}"

curl -s \
	--header "Auth-Token: $CIP_SQUAD_LAB_TOKEN" \
	--form tests='{"build/build": "'$1'"}' \
	--form metadata='{"CI pipeline": "'${CI_PIPELINE_URL}'", "CI job": "'${CI_JOB_URL}'", "arch": "'$BUILD_ARCH'"}' \
	$squad_url
